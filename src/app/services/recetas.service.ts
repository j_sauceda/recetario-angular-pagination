import { Injectable } from '@angular/core';
import { Receta } from '../Receta';
import {
  Firestore,
  addDoc,
  collection,
  deleteDoc,
  doc,
  DocumentSnapshot,
  endBefore,
  getDoc,
  getDocs,
  limit,
  limitToLast,
  orderBy,
  query,
  setDoc,
  startAfter,
  where,
} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class RecetasService {
  page_size = 6;

  constructor(private firestore: Firestore) {}

  get_receta(id_articulo: string) {
    const docRef = doc(this.firestore, 'recetas', id_articulo);
    return getDoc(docRef);
  }

  // Improved method, now with Pagination
  async get_recetas() {
    const docRef = collection(this.firestore, 'recetas');
    const q = query(
      docRef,
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      limit(this.page_size)
    );
    return await getDocs(q);
  }

  // Pagination method
  async next_recetas(last: DocumentSnapshot) {
    const docRef = collection(this.firestore, 'recetas');
    const q = query(
      docRef,
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      startAfter(last),
      limit(this.page_size)
    );
    return await getDocs(q);
  }

  // Pagination method
  async next_recetas_exists(last: DocumentSnapshot) {
    const docRef = collection(this.firestore, 'recetas');
    const q = query(
      docRef,
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      startAfter(last),
      limit(1)
    );
    return await getDocs(q);
  }

  // Pagination method
  async previous_recetas(first: DocumentSnapshot) {
    const docRef = collection(this.firestore, 'recetas');
    const q = query(
      docRef,
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      endBefore(first),
      limitToLast(this.page_size)
    );
    return await getDocs(q);
  }

  // Improved method, now with Pagination
  async get_recetas_autor(id_autor: string) {
    const q = query(
      collection(this.firestore, 'recetas'),
      where('autor', '==', id_autor),
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      limit(this.page_size)
    );
    return await getDocs(q);
  }

  // Pagination method
  async next_recetas_autor(id_autor: string, last: DocumentSnapshot) {
    const q = query(
      collection(this.firestore, 'recetas'),
      where('autor', '==', id_autor),
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      startAfter(last),
      limit(this.page_size)
    );
    return await getDocs(q);
  }

  // Pagination method
  async next_recetas_autor_exists(id_autor: string, last: DocumentSnapshot) {
    const q = query(
      collection(this.firestore, 'recetas'),
      where('autor', '==', id_autor),
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      startAfter(last),
      limit(1)
    );
    return await getDocs(q);
  }

  // Pagination method
  async previous_recetas_autor(id_autor: string, first: DocumentSnapshot) {
    const q = query(
      collection(this.firestore, 'recetas'),
      where('autor', '==', id_autor),
      orderBy('editada', 'asc'),
      orderBy('nombre', 'asc'),
      endBefore(first),
      limitToLast(this.page_size)
    );
    return await getDocs(q);
  }

  add_receta(nueva_receta: Receta) {
    const recetaRef = collection(this.firestore, 'recetas');
    return addDoc(recetaRef, nueva_receta);
  }

  update_receta(receta: Receta) {
    console.log(`Update receta.id: ${receta.id}`);
    const recetaRef = doc(this.firestore, `recetas/${receta.id}`);
    return setDoc(recetaRef, { editada: new Date(), ...receta });
  }

  delete_receta(receta: Receta) {
    const recetaRef = doc(this.firestore, `recetas/${receta.id}`);
    return deleteDoc(recetaRef);
  }
}
