import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-nueva-receta',
  templateUrl: './nueva-receta.component.html',
  styleUrls: ['./nueva-receta.component.css'],
})
export class NuevaRecetaComponent implements OnInit {
  formulario!: FormGroup;
  showPreview = false;
  usuario!: any;

  constructor(
    private fb: FormBuilder,
    private recetasServices: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      introduccion: ['', Validators.required],
      ingredientes: this.fb.array([this.fb.control('')]),
      preparacion: this.fb.array([this.fb.control('')]),
      consejos: this.fb.array([this.fb.control('')]),
      fuente: ['', Validators.required],
    });
    this.usuario = this.userService.get_user();
  }

  onSubmit() {
    this.recetasServices
      .add_receta({
        autor: this.usuario.uid,
        publicada: new Date(),
        editada: new Date(),
        ...this.formulario.value,
      })
      .then((articulo) => {
        console.log(`submitted: ${JSON.stringify(articulo)}`);
        this.router.navigate([`/articulo/${articulo.id}`]);
      })
      .catch((error) => console.error(error));
  }

  get ingredientes() {
    return this.formulario.get('ingredientes') as FormArray;
  }

  add_ingrediente() {
    this.ingredientes.push(this.fb.control(''));
  }

  delete_ingrediente(i: number) {
    this.ingredientes.removeAt(i);
  }

  get preparacion() {
    return this.formulario.get('preparacion') as FormArray;
  }

  add_preparacion() {
    this.preparacion.push(this.fb.control(''));
  }

  delete_preparacion(i: number) {
    this.preparacion.removeAt(i);
  }

  get consejos() {
    return this.formulario.get('consejos') as FormArray;
  }

  add_consejo() {
    this.consejos.push(this.fb.control(''));
  }

  delete_consejo(i: number) {
    this.consejos.removeAt(i);
  }
}
