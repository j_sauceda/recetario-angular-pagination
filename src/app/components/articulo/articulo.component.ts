import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RecetasService } from 'src/app/services/recetas.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css'],
})
export class ArticuloComponent implements OnInit {
  articulo: any = undefined;
  id_articulo!: string;

  constructor(
    private recetasService: RecetasService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id_articulo = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.recetasService.get_receta(this.id_articulo).then((docSnap) => {
      this.articulo = { id: this.id_articulo, ...docSnap.data() };
      // console.log('articulo is: ', this.articulo);
    });
  }
}
