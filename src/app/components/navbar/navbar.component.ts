import { Component, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  @Input() title!: string;

  constructor(private userService: UserService) {}

  is_user_logged_in() {
    return this.userService.is_user_logged_in();
  }
}
