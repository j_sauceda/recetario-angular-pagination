import { Component, OnInit } from '@angular/core';
import { RecetasService } from 'src/app/services/recetas.service';

import {
  DocumentData,
  DocumentSnapshot,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.component.html',
  styleUrls: ['./recetas.component.css'],
})
export class RecetasComponent implements OnInit {
  recetas: any = [];

  // Pagination variables
  docZero!: DocumentSnapshot;
  firstDoc!: DocumentSnapshot;
  lastDoc!: DocumentSnapshot;
  nextPageExists!: boolean;

  constructor(private recetasService: RecetasService) {}

  // Improved method, now with Pagination
  ngOnInit(): void {
    this.recetasService.get_recetas().then((documents) => {
      documents.forEach((docSnap: QueryDocumentSnapshot<DocumentData>) => {
        this.recetas.push({ id: docSnap.id, ...docSnap.data() });
      });
      // Pagination variables
      this.docZero = documents.docs[0];
      this.firstDoc = documents.docs[0];
      this.lastDoc = documents.docs[documents.docs.length - 1];
      this.next_page_exists();
      // For pagination debugging:
      // console.log(`first: ${JSON.stringify(this.firstDoc.id)}`);
      // console.log(`last: ${JSON.stringify(this.lastDoc.id)}`);
      // console.log('recetas contains : ', this.recetas);
    });
  }

  // Pagination method
  get_next_recetas() {
    this.recetasService.next_recetas(this.lastDoc).then((documents) => {
      this.recetas = [];
      documents.forEach((docSnap: QueryDocumentSnapshot<DocumentData>) => {
        this.recetas.push({ id: docSnap.id, ...docSnap.data() });
      });
      // Pagination variables
      this.firstDoc = documents.docs[0];
      this.lastDoc = documents.docs[documents.docs.length - 1];
      this.next_page_exists();
      // For pagination debugging:
      // console.log(`first: ${JSON.stringify(this.firstDoc.id)}`);
      // console.log(`last: ${JSON.stringify(this.lastDoc.id)}`);
    });
  }

  next_page_exists() {
    this.recetasService.next_recetas_exists(this.lastDoc).then((document) => {
      document.empty
        ? (this.nextPageExists = false)
        : (this.nextPageExists = true);
    });
  }

  // Pagination method
  get_previous_recetas() {
    this.recetasService.previous_recetas(this.firstDoc).then((documents) => {
      this.recetas = [];
      documents.forEach((docSnap: QueryDocumentSnapshot<DocumentData>) => {
        this.recetas.push({ id: docSnap.id, ...docSnap.data() });
      });
      // Pagination variables
      this.firstDoc = documents.docs[0];
      this.lastDoc = documents.docs[documents.docs.length - 1];
      this.next_page_exists();
      // For pagination debugging:
      // console.log(`first: ${JSON.stringify(this.firstDoc.id)}`);
      // console.log(`last: ${JSON.stringify(this.lastDoc.id)}`);
    });
  }
}
