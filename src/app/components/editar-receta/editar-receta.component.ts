import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-editar-receta',
  templateUrl: './editar-receta.component.html',
  styleUrls: ['./editar-receta.component.css'],
})
export class EditarRecetaComponent implements OnInit {
  articulo: any = undefined;
  id_articulo!: string;
  formulario!: FormGroup;
  showPreview = false;
  usuario!: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private recetasService: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.id_articulo = this.activatedRoute.snapshot.paramMap.get('id') || '';
    this.recetasService.get_receta(this.id_articulo).then((docSnap) => {
      this.articulo = { id: this.id_articulo, ...docSnap.data() };
      this.setForm();
    });
    this.usuario = this.userService.get_user();
  }

  initForm() {
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      introduccion: ['', Validators.required],
      ingredientes: this.fb.array([this.fb.control('')]),
      preparacion: this.fb.array([this.fb.control('')]),
      consejos: this.fb.array([this.fb.control('')]),
      fuente: ['', Validators.required],
    });
  }

  setForm() {
    const values = {
      nombre: this.articulo.nombre,
      imagen: this.articulo.imagen,
      introduccion: this.articulo.introduccion,
      fuente: this.articulo.fuente,
    };
    this.formulario.patchValue(values);
    this.articulo.ingredientes.forEach((value: string, i: number) => {
      this.ingredientes.insert(i, this.fb.control(value));
    });
    this.delete_ingrediente(this.ingredientes.length - 1);
    this.articulo.preparacion.forEach((value: string, i: number) => {
      this.preparacion.insert(i, this.fb.control(value));
    });
    this.delete_preparacion(this.preparacion.length - 1);
    this.articulo.consejos.forEach((value: string, i: number) => {
      this.consejos.insert(i, this.fb.control(value));
    });
    this.delete_consejo(this.consejos.length - 1);
  }

  onSubmit() {
    this.recetasService
      .update_receta({
        autor: this.articulo.autor,
        id: this.id_articulo,
        publicada: this.articulo.publicada,
        editada: new Date(),
        ...this.formulario.value,
      })
      .then(() => {
        this.router.navigate([`/articulo/${this.id_articulo}`]);
      })
      .catch((error) => console.error(error));
  }

  get ingredientes() {
    return this.formulario.get('ingredientes') as FormArray;
  }

  add_ingrediente() {
    this.ingredientes.push(this.fb.control(''));
  }

  delete_ingrediente(i: number) {
    this.ingredientes.removeAt(i);
  }

  get preparacion() {
    return this.formulario.get('preparacion') as FormArray;
  }

  add_preparacion() {
    this.preparacion.push(this.fb.control(''));
  }

  delete_preparacion(i: number) {
    this.preparacion.removeAt(i);
  }

  get consejos() {
    return this.formulario.get('consejos') as FormArray;
  }

  add_consejo() {
    this.consejos.push(this.fb.control(''));
  }

  delete_consejo(i: number) {
    this.consejos.removeAt(i);
  }
}
