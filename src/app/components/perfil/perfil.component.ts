import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Receta } from 'src/app/Receta';
import { PerfilService } from 'src/app/services/perfil.service';
import { RecetasService } from 'src/app/services/recetas.service';
import { UserService } from 'src/app/services/user.service';

import { sample_recetas } from 'src/app/sample-recetas';
import {
  DocumentData,
  DocumentSnapshot,
  QueryDocumentSnapshot,
} from '@angular/fire/firestore';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
})
export class PerfilComponent implements OnInit {
  formulario!: FormGroup;
  perfil!: any;
  recetas: any = [];
  usuario!: any;
  sample_recetas!: Receta[];

  // Pagination variables
  docZero!: DocumentSnapshot;
  firstDoc!: DocumentSnapshot;
  lastDoc!: DocumentSnapshot;
  nextPageExists!: boolean;

  constructor(
    private fb: FormBuilder,
    private perfilService: PerfilService,
    private recetasService: RecetasService,
    private router: Router,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.usuario = this.userService.get_user();
    this.get_perfil();
    this.get_recetas_perfil(); // Pagination variable init
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      imagen: ['', Validators.required],
      acerca: [''],
    });
    this.sample_recetas = sample_recetas;
  }

  get_perfil() {
    this.perfilService.get_perfil(this.usuario.uid).then((docs) => {
      docs.forEach((docSnap) => {
        this.perfil = docSnap.data();
      });
    });
  }

  // Improved method, now with Pagination
  get_recetas_perfil() {
    this.recetasService
      .get_recetas_autor(this.usuario.uid)
      .then((documents) => {
        documents.forEach((docSnap: QueryDocumentSnapshot<DocumentData>) => {
          this.recetas.push({ id: docSnap.id, ...docSnap.data() });
        });
        // Pagination variables
        this.docZero = documents.docs[0];
        this.firstDoc = documents.docs[0];
        this.lastDoc = documents.docs[documents.docs.length - 1];
        this.next_page_exists();
        // For pagination debugging:
        // console.log(`first: ${JSON.stringify(this.firstDoc.id)}`);
        // console.log(`last: ${JSON.stringify(this.lastDoc.id)}`);
      });
  }

  // Pagination method
  get_next_recetas_perfil() {
    this.recetasService
      .next_recetas_autor(this.usuario.uid, this.lastDoc)
      .then((documents) => {
        this.recetas = [];
        documents.forEach((docSnap: QueryDocumentSnapshot<DocumentData>) => {
          this.recetas.push({ id: docSnap.id, ...docSnap.data() });
        });
        // Pagination variables
        this.firstDoc = documents.docs[0];
        this.lastDoc = documents.docs[documents.docs.length - 1];
        this.next_page_exists();
        // For pagination debugging:
        // console.log(`first: ${JSON.stringify(this.firstDoc.id)}`);
        // console.log(`last: ${JSON.stringify(this.lastDoc.id)}`);
      });
  }

  next_page_exists() {
    this.recetasService
      .next_recetas_autor_exists(this.usuario.uid, this.lastDoc)
      .then((document) => {
        document.empty
          ? (this.nextPageExists = false)
          : (this.nextPageExists = true);
      });
  }

  // Pagination method
  get_previous_recetas_perfil() {
    this.recetasService
      .previous_recetas_autor(this.usuario.uid, this.firstDoc)
      .then((documents) => {
        this.recetas = [];
        documents.forEach((docSnap: QueryDocumentSnapshot<DocumentData>) => {
          this.recetas.push({ id: docSnap.id, ...docSnap.data() });
        });
        // Pagination variables
        this.firstDoc = documents.docs[0];
        this.lastDoc = documents.docs[documents.docs.length - 1];
        this.next_page_exists();
        // For pagination debugging:
        // console.log(`first: ${JSON.stringify(this.firstDoc.id)}`);
        // console.log(`last: ${JSON.stringify(this.lastDoc.id)}`);
      });
  }

  onSubmit() {
    this.perfilService
      .add_perfil({
        user: this.usuario.uid,
        ...this.formulario.value,
      })
      .then((new_perfil) => {
        console.log(`Submitted profile: ${JSON.stringify(new_perfil)}`);
        this.get_perfil();
      })
      .catch((error) => console.error(error));
  }

  load_sample_recetas() {
    this.sample_recetas.forEach((receta) => {
      this.recetasService
        .add_receta({
          publicada: new Date(),
          editada: new Date(),
          ...receta,
          autor: this.usuario.uid,
        })
        .then(() => {
          console.log(`submitted: ${receta.nombre}`);
        })
        .catch((error) => console.error(error));
    });
    this.get_recetas_perfil();
  }

  onUpdate(receta: Receta) {
    this.router.navigate([`/editar-receta/${receta.id}`]);
  }

  onDelete(receta: Receta) {
    this.recetasService.delete_receta(receta).then(() => {
      this.get_recetas_perfil();
      console.log('Receta deleted successfully');
    });
  }
}
