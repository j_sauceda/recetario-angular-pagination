export interface Receta {
  id?: string;
  autor: string;
  nombre: string;
  imagen: string;
  editada?: Date;
  publicada?: Date;
  introduccion: string;
  ingredientes: string[];
  preparacion: string[];
  consejos: string[];
  fuente: string;
}
